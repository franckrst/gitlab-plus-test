<div align="center">

# 🐧 GitLab +

**Add some features to your gitlab with a simple chrome extension**
*(wip project)*


</div>

# ⭐ Feature

## Merge Request

![MR-demo](doc/assets/demo.png)
### Lighthouse

Create an artifact `.gtp/lighthouse-XXXX.json` in your CI.

Gitlab+ compares the CI artifacts between branches `from` and `to`.


### Generic metrics

Create an artifact `.gtp/metrics-<*>.json` in your CI. All JSON files are concatenated.

*[Json-Schema]()*
```json
[{
    "label" : "⚖️ Analyse NPM", // required
    "status": "success",
    "link" : "http://google.com",
    "linkTitle" : "Open Report",
    "metrics": [{
        "title" : "Taille de la version compilée",  // required
        "value": "26", 
        "unit": "MegaOctet"
    }]
}]
```

### Compare

Gitlab+ compares CI artifacts with name `compare-metric-*.json` between branches `from` and `to`.

```json
[{
    "label" : "⚖️ Analyse NPM", // required
    "status": "success",
    "link" : "http://google.com",
    "linkTitle" : "Open Report",
    "metrics": [{
        "title" : "Taille de la version compilée",  // required
        "best" : "more|less",
        "value": "26",
        "unit": "MegaOctet"
    }]
}]
```


## Todo

### Feature

- [ ] Show badge of repo in repository list
- [ ] Add feature activation/deactivation

### Other
- [X] implement [JSON-Schema](https://json-schema.org/) validator
- [ ] implement gitlab dark theme
- [ ] add README dev information
- [ ] add cache
